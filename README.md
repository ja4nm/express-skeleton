# Express typescript skeleton

By: Jan M, nov 2020

This skeleton project includes:
- express.js framework (routing, versioning, basic middleware, error handling, ...)
- PostgreSQL database configuration (model repository pattern, database migrations)
- Swagger documentation
- Mocha tests
- Docker configuration files
- Gulp and Eslint configuration

## Getting started

* Copy `config.sample.json` to `config.json` and modify the setting inside
* Check the database settings in `compose-local.yml`, to match the ones in a `config.json` file
* Run `npm run start`
* Run `docker-compose -f compose-local.yml up`

Note: If you wish to use different configuration file, you can set CONFIG_FILE environment variable when running the application.

**Run application for development (with nodemon):**

    ./docker-tools.sh local
    or
    docker-compose -f compose-local.yml up

**Run tests:**

    ./docker-tools.sh test
    or
    docker-compose -f compose-test.yml rm -f && docker-compose -f compose-test.yml up --abort-on-container-exit --exit-code-from skeleton_node

**Open shell console inside docker:**

    ./docker-tools.sh shell
    or
    docker exec -ti skeleton_node /bin/bash


**Run application for production**

Create a file `config.prod.json` and set the database settings inside. Set the setting `swagger: false` for security reasons.
You probably wanna connect to the database which is already running on a remote server or in separate docker container.

Create a file `compose-prod.yml`:

    version: "3.2"
    services:
      skeleton_node:
        depends_on:
          - test_skeleton_db
        build:
          context: .
          dockerfile: Dockerfile
        command: npm run test
        environment:
          ENVIRONMENT: prod
          CONFIG_FILE: config.json
        networks:
          - backend
        links:
          - test_skeleton_db:database
        volumes:
          - ./config.prod.json:/usr/src/app/config.json
        container_name: skeleton_node
    networks:
      backend:
        driver: bridge

Run `docker-compose -f compose-prod.yml up --build`

Note: It may be wise to move production configuration files out of project directory, as pushing them to Git can be a security risk.

## Database migrations

(Inspired by: https://www.npmjs.com/package/postgres-migrations)

You can define database migrations inside `src/db/migrations/` folder. Each migration should follow strict naming pattern 
`[5 digit id]_[name][.ts or .sql]` (i.e. `00001_sample.sql`). Migrations ids should be unique, as migrations are executed one by one
in order of their ids. Each migration is executed in a transaction. Either it completes successfully, or it is rolled back and the process is aborted.
Previously run migration scripts shouldn not be modified, since we want the process to be repeated in the same way for every new environment. 
This is enforced by hashing the file contents of a migration script and storing this in migrations table. There is deliberately no concept of
a "down" migration. If you need to reverse something, you can just add another migration which negates the previous one.

If your migration migration contains complex logic, you can also define migration as a typescript file. To ensure that migration
is run in a transaction, each query must be executed on a `client` object provided as parameter to `migrate()` method. If you do
not want your migration to be executed in a transaction, you can overwrite method `transactionMode()` to return false.

    export const migration = new class extends Migration {
    
         public transactionMode(): boolean {
            return true;
         }
    
        async migrate(client: PoolClient, db: Database): Promise<void> {
            await db.query({
                client: client,
                query: 'ALTER TABLE users ADD COLUMN password VARCHAR '
            });
        }
    
    };


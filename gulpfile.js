const gulp = require("gulp");
const eslint = require("gulp-eslint");
const shell = require("gulp-shell");
const mocha = require("gulp-mocha");
const gulpIf = require("gulp-if");


const isFixed = (file) => {
    return file.eslint != null && file.eslint.fixed;
};

const lintTask = (fix = false) => {
    return gulp
        .src([
            "./src/**/*.ts",
            "./test/**/*.ts",
            //"!./src/db/migrations/*.ts"
        ], { base: "./" })
        .pipe(eslint({ fix: fix }))
        .pipe(eslint.format())
        .pipe(gulpIf(isFixed, gulp.dest("./")))
        .pipe(eslint.failAfterError());
};


gulp.task("lint", () => {
    return lintTask(false);
});

gulp.task("lint:fix", () => {
    return lintTask(true);
});

gulp.task("copyNonTs", () => {
    return gulp
        .src([
            "./src/**/*.sql",
            "./src/**/*.twig",
            "./src/**/*.json",
            "./src/**/*.html"
        ])
        .pipe(gulp.dest("dist/"));
});

gulp.task("build", gulp.series(
    "copyNonTs",
    shell.task("tsc")
));

gulp.task("test", () => {
    return gulp
        .src([
            "./test/index.ts"
        ])
        .pipe(mocha({
            timeout: 5000,
            reporter: "spec",
            require: ["ts-node/register"]
        }));
});

gulp.task("default", gulp.series(
    "lint",
    "build"
));

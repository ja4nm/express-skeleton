FROM node:14

WORKDIR /usr/src/app

COPY package*.json tsconfig.json gulpfile.js ./
COPY src/ ./src/

RUN npm install
RUN npm run build
RUN rm -rf src/ tsconfig.json

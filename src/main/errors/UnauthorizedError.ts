import GenericError from "./GenericError";

export default class UnauthorizedError extends GenericError {

    constructor(message?: string) {
        if (!message) message = "User is not authorized.";
        super("AuthorizedError", message);
    }

}

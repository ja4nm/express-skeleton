
export default class GenericError extends Error {

    /**
     * Each error should extend this class.
     *
     * @param name: error name, usually just Error class name.
     * @param message: error message.
     */
    constructor(name: string, message: string) {
        super(message);
        this.name = name;
    }

}

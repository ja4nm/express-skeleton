import GenericError from "./GenericError";

export default class ForbiddenError extends GenericError {

    constructor(message?: string) {
        if (!message) message = "User does not have permission to access this resource.";
        super("ForbiddenError", message);
    }

}

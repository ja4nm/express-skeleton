import * as express from "express";

/**
 * Main routes configuration.
 */
export default class RoutesConfig {

    // api versions available (i.e. v1, v2)
    public versions: string[];
    // each api version should have its own router
    public routers: express.Router[];
    // active api version index (if v2 is active, /v2/ping = /ping)
    public activeVersion: number;

}

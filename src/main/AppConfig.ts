
/**
 * Main application config interface.
 * It should have the same structure as sample.config.json file.
 */
export interface AppConfig {
    port: number;
    swagger: boolean;
    db: DbConfig;
}

export interface DbConfig {
    url: string;
    maxConnections?: number;
    idleTimeout?: number;
    connectTimeout?: number;
    logging?: boolean;
}


/**
 * JsonResponse data class. Each api endpoint which returns a
 * Content-Type: text/json, should usually have this format.
 */
export default class JsonResponse {

    private type: string;
    public name: string;
    public data: any;

    private constructor(type: string, name: string, data: any) {
        this.type = type;
        this.name = name;
        this.data = data;
    }

    public static ok(name: string, data: unknown): JsonResponse {
        return new JsonResponse("ok", name, data);
    }

    public static err(name: string, message: string): JsonResponse {
        return new JsonResponse("err", name, { message: message });
    }

    public static throwing(e: Error): JsonResponse {
        return new JsonResponse("err", e.name, { message: e.message });
    }

}

import { Request, Response } from "express";
import UnauthorizedError from "../errors/UnauthorizedError";

/**
 * Bearer auth middleware. It should check if request contains a valid Authorization token.
 * (This is just an example, you must provide implementation by yourself.)
 */
export const bearerAuth = function(req: Request, res: Response, next: () => void): void {
    const token = req.header("Authorization");

    // validate token here
    if (token) {
        res.locals.user = "user";
        next();
    } else {
        throw new UnauthorizedError();
    }
};

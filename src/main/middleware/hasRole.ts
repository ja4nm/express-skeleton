import { bearerAuth } from "./bearerAuth";
import { Request, Response } from "express";
import ForbiddenError from "../errors/ForbiddenError";

/**
 * Check if user has a certain role permission.
 * (This is just an example, you must provide implementation by yourself.)
 *
 * @param role: role name.
 */
export const hasRole = function (role: string): Array<(req: Request, res: Response, next: () => void) => void> {
    const f = (req: Request, res: Response, next: () => void): void => {
        const user = res.locals.user;

        // check role here
        if (user === role){
            next();
        } else {
            throw new ForbiddenError();
        }
    };

    return [bearerAuth, f];
};

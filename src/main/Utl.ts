
export default class Utl {

    /**
     * Sleep for x milliseconds.
     *
     * @param time: time in milliseconds.
     */
    public static async sleep(time: number): Promise<void> {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, time);
        });
    }

    /**
     * Wait for all promises to finish, regardless if they are rejected or resolved.
     *
     * @param pool: pool of promises to wait for.
     * @param rejectHandler: what should a rejected promise return.
     *                       By default it returns an exception which was thrown.
     */
    public static async promiseResolveAll(pool: Promise<any>[], rejectHandler?: (e: Error) => any): Promise<any> {
        const handler = rejectHandler? rejectHandler : (e: Error) => e;

        return await Promise.all(pool.map(p => p.catch(e => {
            return handler(e);
        })));
    }

    /**
     * Create a copy of an object.
     */
    public static clone(obj: object): object {
        return Object.assign({}, obj);
    }

    /**
     * Pad the string from right side.
     *
     * @param str: string to pad.
     * @param n: length of padded string.
     * @param chr: char to add for padding.
     */
    public static padRight(str: string, n: number, chr = " "): string {
        const pad = Math.max(0, n-str.length);
        return str+chr.repeat(pad);
    }

    /**
     * Pad the string from left side.
     *
     * @param str: string to pad.
     * @param n: length of padded string.
     * @param chr: char to add for padding.
     */
    public static padLeft(str: string, n: number, chr = " "): string {
        const pad = Math.max(0, n-str.length);
        return chr.repeat(pad)+str;
    }

}

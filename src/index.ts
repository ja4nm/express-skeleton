import { routesConfig } from "./routes";
import { app } from "./App";

app.start(routesConfig).then((addr) => {
    console.log("API is now listening on address " + addr);
}).catch((e) => {
    console.log("Failed to start API: " + e.message);
});

import * as bodyParser from "body-parser";
import * as compression from "compression";
import * as cors from "cors";
import * as express from "express";
import * as logger from "morgan";
import * as fs from "fs";
import * as path from "path";
import Database from "./db/Database";
import * as http from "http";
import RoutesConfig from "./main/RoutesConfig";
import * as commentJson from "comment-json";
import * as swaggerJsDoc from "swagger-jsdoc";
import * as swaggerUi from "swagger-ui-express";
import JsonResponse from "./main/JsonResponse";
import MigrationManager from "./db/MigrationManager";
import { AppConfig } from "./main/AppConfig";

export class App {

    private _express: express.Application;
    private _server: http.Server;
    private _config: AppConfig;
    private _baseUrl: string;
    private _apiVersion: string;
    private _db: Database;


    /**
     * Some getters
     */
    get config(): any {
        return this._config;
    }
    get baseUrl(): string {
        return this._baseUrl;
    }
    get apiVersion(): string {
        return this._apiVersion;
    }
    get db(): Database {
        return this._db;
    }


    /**
     * Connect to database and start server.
     *
     * @param: appConfig: application config object, if null it is read from config.json file.
     */
    public async start(routesConfig: RoutesConfig, appConfig?: AppConfig): Promise<string> {
        return new Promise((resolve, reject) => {
            try {
                this._config = appConfig? appConfig : App.readConfig();
            }catch (e) {
                reject(e);
                return;
            }

            const port = process.env.PORT || this.config.port || 3000;

            // init express
            this._express = express();
            this._express.use(bodyParser.json());
            this._express.use(bodyParser.urlencoded({ extended: false }));
            this._express.set("json spaces", 4);
            this._express.use(logger("common"));
            this._express.use(cors());
            this._express.use(compression());
            this._express.set("views", __dirname + "/views");
            this._express.set("view engine", "twig");
            this._express.set("twig options", { strict_variables: false });

            // init routes and versioning
            this.initRoutes(routesConfig, this._express);

            // error handler
            this._express.use(App.errorHandler);

            // connect to the database and start server
            this.initDatabase().then(() => {
                this._server = this._express.listen(port, () => {
                    this._baseUrl = "http://localhost:"+port;
                    resolve(this._baseUrl);
                });
            }).catch((e) => {
                reject(e);
            });
        });
    }

    /**
     * Close database connections and stop server if its running.
     */
    public async stop(): Promise<void> {
        if (this._db){
            await this._db.close();
        }
        if (this._server) {
            this._server.close();
        }
    }


    /**
     * Read config.json file. Get path to file from CONFIG_FILE environment variable.
     */
    private static readConfig(): AppConfig {
        let file = process.env.CONFIG_FILE;
        if (!file) file = "config.json";

        if (!fs.existsSync(file)){
            throw new Error("Config file "+file+" does not exists.");
        }

        try{
            const data = fs.readFileSync(file, "utf-8");
            return commentJson.parse(data, undefined, true);
        }catch (e) {
            throw new Error("Failed to parse config file: "+file);
        }
    }

    /**
     * Initialise routes, API versioning and swagger documentation for each API version.
     * Example: v2 extends v1 (v2 contains all routes from v1 and overwrite new ones).
     */
    private initRoutes(routerConfig: RoutesConfig, router: express.Router){
        this._apiVersion = routerConfig.versions[routerConfig.activeVersion];

        for (let i = 0; i < routerConfig.versions.length; i++){
            const v = routerConfig.versions[i];

            // version N should extend versions 0, 1, ..., N-1
            const r = express.Router();
            for(let j = i; j >= 0; j--){
                r.use(routerConfig.routers[j]);
            }

            // each version should contain a swagger endpoint at /vX/docs
            if (this._config.swagger) {
                App.initSwagger("/"+v, r, routerConfig, i);
            }
            router.use("/"+v, r);

            // swagger endpoint at /docs
            if(i == routerConfig.activeVersion){
                if (this._config.swagger) {
                    App.initSwagger("/", router, routerConfig, i);
                }
                router.use("/", r);
            }
        }
    }

    /**
     * Initialise and connect to postgres database. Validate and execute migrations.
     */
    private async initDatabase(){
        const {url, maxConnections, idleTimeout, connectTimeout, logging} = this._config.db;

        // initialise to the database
        this._db = new Database({
            connectionString: url,
            max: maxConnections,
            idleTimeoutMillis: idleTimeout,
            connectionTimeoutMillis: connectTimeout,
            log: (logging)? (msg) => { console.log(msg); } : undefined
        });

        // try to connect to database
        if(!await this._db.ping()){
            throw new Error("Failed to ping the database.");
        }

        // execute database migrations
        console.log("Validating and executing database migrations.");
        const manager = new MigrationManager(this._db, path.join(__dirname, "./db/migrations"));
        manager.setVerbose(true);
        if(await manager.migrate()){
            const last = await manager.getLastAppliedMigration();
            const id = (last)? last.id : 0;
            console.log("Database is now at version "+id+".");
        } else {
            throw new Error("Database migrations failed.");
        }
    }

    /**
     * Init swagger for one API version at endpoint /docs.
     */
    private static initSwagger(base: string, router: express.Router, routesConfig: RoutesConfig, versionIndex): void {
        if(base.substring(base.length-1) == "/") base = base.substring(base.length-1);

        const swaggerDefinition = {
            info: {
                title: process.env.npm_package_name+" API",
                version: routesConfig.versions[versionIndex],
                description: process.env.npm_package_description
            },
            basePath: base
        };

        const apis: string[] = [];
        for(let i = 0; i <= versionIndex; i++){
            apis.push("./src/routes/"+routesConfig.versions[i]+"/**/*.ts");
        }
        const options = {
            swaggerDefinition,
            apis: apis
        };
        const swaggerDocs = swaggerJsDoc(options);
        router.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
    }

    /**
     * Express error handler. This function is executed when an exception is thrown inside endpoint.
     */
    private static errorHandler(err: Error, req: express.Request, res: express.Response, next) {
        res.status(500);
        res.header("Content-Type","application/json");
        console.log(err);
        res.send(JsonResponse.throwing(err));
    }

}

export const app = new App();

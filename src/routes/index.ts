import { Router } from "express";
import * as fs from "fs";
import * as path from "path";
import RoutesConfig from "../main/RoutesConfig";

export const routesConfig = new RoutesConfig();

// Main router versions configuration.
routesConfig.activeVersion = 1;
routesConfig.versions = ["v1", "v2"];
routesConfig.routers = [];

// for each api version
for (let i = 0; i < routesConfig.versions.length; i++) {
    const v = routesConfig.versions[i];
    const dir: string = path.join(__dirname, v);
    const router = Router();

    // include every route .routes.ts file in the folder vX/ and add the
    // router to routers array
    fs.readdirSync(dir).forEach( async (file) => {
        if (file.endsWith(".routes.ts")) {
            router.use((await import(dir+"/"+file)).router);
        }
    });

    routesConfig.routers.push(router);
}


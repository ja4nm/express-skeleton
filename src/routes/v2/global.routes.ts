import { Router, Request, Response } from "express";

const router = Router();

/**
 * @swagger
 * path:
 *  /version:
 *    get:
 *      description: Returns API version number. Version 2 (example).
 *      tags: [Global]
 *      responses:
 *        "200":
 *          description: OK
 */
router.get("/version", (req: Request, res: Response) => {
    res.json(2);
});


export { router };

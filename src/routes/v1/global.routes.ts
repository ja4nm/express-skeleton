import { Router, Request, Response } from "express";
import JsonResponse from "../../main/JsonResponse";

const router = Router();

/**
 * @swagger
 * path:
 *  /:
 *    get:
 *      description: Root endpoint. Should return a welcome message.
 *      tags: [Global]
 *      responses:
 *        "200":
 *          description: OK
 */
router.get("/", (req: Request, res: Response) => {
    res.send(JsonResponse.ok("hello", "Hello world!"));
});

/**
 * @swagger
 * path:
 *  /ping:
 *    get:
 *      description: Should return pong.
 *      tags: [Global]
 *      responses:
 *        "200":
 *          description: OK
 */
router.get("/ping", (req: Request, res: Response) => {
    res.header("Content-Type", "text/plain");
    res.send("pong");
});

/**
 * @swagger
 * path:
 *  /version:
 *    get:
 *      description: Returns API version number.
 *      tags: [Global]
 *      responses:
 *        "200":
 *          description: OK
 */
router.get("/version", (req: Request, res: Response) => {
    res.json(1);
});

/**
 * @swagger
 * path:
 *  /twig:
 *    get:
 *      description: Test twig template endpoint.
 *      tags: [Global]
 *      responses:
 *        "200":
 *          description: OK
 */
router.get("/twig", (req: Request, res: Response) => {
    res.header("Content-Type", "text/html");
    res.render("test", {
        name : "Twig"
    });
});

export { router };

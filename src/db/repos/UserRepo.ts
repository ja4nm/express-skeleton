import ModelRepo from "../ModelRepo";
import User from "../models/User";

export default class UserRepo extends ModelRepo<User> {

    get tableName(): string {
        return "users";
    }

    get type(): any {
        return User;
    }

}

import Database from "../Database";
import UserRepo from "./UserRepo";

export default class RepoIndex {

    constructor(private db: Database){}

    // Initialise each repository here.
    // Any repository defined here, can be accessed through App class, i.e.: app.db.repos.userRepo.

    public userRepo = new UserRepo(this.db);

}

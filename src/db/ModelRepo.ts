import Repo from "./Repo";
import Model from "./Model";
import PromisePool from "native-promise-pool";
import Utl from "../main/Utl";

export default abstract class ModelRepo<T extends Model> extends Repo {

    /**
     * Should return model type i.e. return User.
     */
    public abstract get type(): any;

    /**
     * Should return database table name of a model.
     */
    public abstract get tableName(): string;

    /**
     * Should return primary key in that table.
     */
    public get primaryKey(): string{
        return "id";
    }

    /**
     * Returns array of all records in a table.
     */
    public async findAll(): Promise<T[]> {
        return await this.db.query({
            query: `SELECT * FROM ${this.tableName}`,
            model: this.type,
            fetch: "rows",
            errorHandler: () => []
        });
    }

    /**
     * Finds a model by primary key / id.
     */
    public async findById(id: number | string): Promise<T> {
        return await this.db.query({
            query: `SELECT * FROM ${this.tableName} WHERE ${this.primaryKey}=$1`,
            values: [id],
            model: this.type,
            fetch: "first",
            errorHandler: () => null
        });
    }

    /**
     * Insert one or more models. If all models are successfully inserted, their ids will
     * be updated with the ones in the database.
     *
     * @param: model: model(s) to insert.
     * @param: conflictIgnore: if set to true, a conflict on primary key will be ignored.
     * @return: number of inserted models.
     */
    public async insert(model: T | T[], conflictIgnore = false): Promise<number> {
        const objects = Array.isArray(model)? model : [model];
        let rowCount = 0;

        const r = await this.db.insert({
            table: this.tableName,
            objects: objects,
            conflictIgnoreOn: conflictIgnore? this.primaryKey : undefined,
            returning: this.primaryKey,
            errorHandler: () => null
        });

        if (r){
            rowCount = r.rowCount;
            if (r.rows.length == objects.length){
                for (let i = 0; i < objects.length; i++) {
                    objects[i][this.primaryKey] = r.rows[i][this.primaryKey];
                }
            }
        }

        return rowCount;
    }

    /**
     * Insert on or more models. If a model already exists, update it.
     * This method uses multiple threads to update models. There is no guarantee of order of insertions.
     * If model is successfully saved, its id will be updated with the one in the database.
     *
     * @param model: model(s) to insert or update.
     * @return: number of saved models.
     */
    public async save(model: T | T[]): Promise<number> {
        let rowCount = 0;
        let client;

        try{
            client = await this.db.connect();
            const arr = Array.isArray(model)? model : [model];
            const pool = new PromisePool(this.db.maxThreads);
            const queue: Promise<any>[] = [];

            for (let i = 0; i < arr.length; i++){
                const p = pool.open(async () => {
                    const r = await this.db.insert({
                        client: client,
                        table: this.tableName,
                        objects: [arr[i]],
                        conflictUpdateOn: this.primaryKey,
                        returning: this.primaryKey
                    });

                    if (r.rows.length > 0) arr[i][this.primaryKey] = r.rows[0][this.primaryKey];
                    return r.rowCount;
                });
                queue.push(p);
            }

            const results = await Utl.promiseResolveAll(queue, () => 0);
            for (const c of results) rowCount += c;
        } catch (e) {
            // ignored
        }

        if (client) client.release();
        return rowCount;
    }

    /**
     * Update one or more models in the database.
     * This method uses multiple threads to update models.
     *
     * @param: model: model(s) to update.
     * @return: number of updated models.
     */
    public async update(model: T | T[]): Promise<number> {
        let rowCount = 0;
        let client;

        try{
            client = await this.db.connect();
            const arr = Array.isArray(model)? model : [model];
            const pool = new PromisePool(this.db.maxThreads);
            const queue: Promise<any>[] = [];

            for (let i = 0; i < arr.length; i++){
                const p = pool.open(async () => {
                    const obj = Utl.clone(arr[i]);
                    const id = obj[this.primaryKey];
                    delete obj[this.primaryKey];

                    const r = await this.db.update({
                        client: client,
                        table: this.tableName,
                        object: arr[i],
                        where: `${this.primaryKey}=$1`,
                        whereArgs: [id]
                    });
                    return r.rowCount;
                });
                queue.push(p);
            }

            const results = await Utl.promiseResolveAll(queue, () => 0);
            for (const c of results) rowCount += c;
        } catch (e) {
            // ignored
        }

        if (client) client.release();
        return rowCount;
    }

    /**
     * Delete one or more models.
     *
     * @param: model(s) to delete.
     * @return: number of deleted models.
     */
    public async delete(model: T | T[]): Promise<number> {
        const arr = Array.isArray(model)? model : [model];
        if (arr.length == 0) return 0;

        let rowCount = 0;
        try{
            const client = await this.db.connect();
            const ids: any[] = [];
            for (const m of arr) ids.push(m[this.primaryKey]);

            const r = await this.db.delete({
                client: client,
                table: this.tableName,
                column: this.primaryKey,
                values: ids
            });

            rowCount = r.rowCount;
            client.release();
        }catch (e) {
            // ignored
        }

        return rowCount;
    }

}


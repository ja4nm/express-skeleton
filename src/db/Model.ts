
export default abstract class Model {

    /**
     * Create new model from object properties.
     */
    constructor(obj: object) {
        if (obj) Object.assign(this, obj);
    }

    /**
     * Convert array of generic objects to array of Model object.
     *
     * @param model: model type i.e. User
     * @param objects: array of objects
     */
    public static toModelList(model: { new(obj): Model }, objects: object[]): Model[] {
        const list: Model[] = [];
        for (const o of objects) {
            list.push(new model(o));
        }
        return list;
    }

}

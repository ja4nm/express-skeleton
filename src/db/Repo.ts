import Database from "./Database";

export default abstract class Repo {

    protected readonly db: Database;

    /**
     * Abstract repository class. Each repository should extend this class.
     *
     * @param db: Database instance to use.
     */
    constructor(db: Database){
        this.db = db;
    }

}

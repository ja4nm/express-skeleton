import { Pool, PoolClient, PoolConfig } from "pg";
import Model from "./Model";
import * as Cursor from "pg-cursor";
import * as util from "util";
import GenericError from "../main/errors/GenericError";
import RepoIndex from "./repos";

export default class Database {

    private readonly _pool: Pool;
    private readonly _maxThreads: number;
    private readonly _repoIndex: RepoIndex;

    constructor(options: PoolConfig){
        this._pool = new Pool(options);
        this._maxThreads = Math.max(5, this._pool.getMaxListeners());
        this._repoIndex = new RepoIndex(this);
    }


    get pool(): Pool {
        return this._pool;
    }

    get repo(): RepoIndex {
        return this._repoIndex;
    }

    /**
     * Max number of threads when doing bulk database tasks (i.e. bulk update).
     */
    get maxThreads(): number {
        return this._maxThreads;
    }


    /**
     * Get next free connection from connection pool.
     */
    public async connect(): Promise<PoolClient> {
        return await this.pool.connect();
    }

    /**
     * Close all connections in connection pool.
     */
    public async close(): Promise<void> {
        await this._pool.end();
    }

    /**
     * Ping the database.
     *
     * @return: returns true if ping was successful.
     */
    public async ping(): Promise<boolean> {
        try{
            await this.pool.query("SELECT NOW()");
        }catch (e) {
            return false;
        }
        return true;
    }

    /**
     * Helper method to build an update query clause.
     *
     * @param values: update query values will be added to this array.
     * @param obj: object to update.
     * @param where: where clause.
     * @param whereArgs: where clause arguments.
     */
    private prepareUpdateClause(values: any[], obj: object, where = "", whereArgs: any[] = []): string {
        const sets: string[] = [];
        const entries = Object.entries(obj);

        for (const v of whereArgs) {
            values.push(v);
        }

        for (let i = 0; i < entries.length; i++){
            sets.push(`${entries[i][0]}=$${values.length+1}`);
            values.push(entries[i][1]);
        }

        let q = "SET "+sets.join(",");
        if(where) q += " WHERE "+where;
        return q;
    }

    /**
     * Execute a query with given query options.
     *
     * @param opt: look QueryOptions interface for info.
     */
    public async query(opt: QueryOptions): Promise<any> {
        let client = opt.client;
        let release = false;
        let res: any = null;

        try {
            if (!client) {
                client = await this.connect();
                release = true;
            }

            if (opt.fetch == "cursor") {
                res = await client.query(new Cursor(opt.query, opt.values));
            } else {
                const r = await client.query(opt.query, opt.values);

                if (opt.fetch == "rows") {
                    if (opt.model) r.rows = Model.toModelList(opt.model, r.rows);
                    res = r.rows;
                } else if (opt.fetch == "first") {
                    let model: any = null;
                    if (r.rows.length > 0) {
                        if (opt.model) model = Model.toModelList(opt.model, [r.rows[0]])[0];
                        else model = r.rows[0];
                    }
                    res = model;
                } else if (opt.fetch == "count") {
                    res = r.rowCount;
                } else {
                    if (opt.model) r.rows = Model.toModelList(opt.model, r.rows);
                    res = r;
                }
            }
        }catch (e) {
            if (opt.errorHandler) {
                res = opt.errorHandler(e);
            } else {
                throw e;
            }
        }

        if (client && release) client.release();
        return res;
    }

    /**
     * Insert new row(s) into the table.
     *
     * @param opt: look InsertOptions interface for info.
     */
    public async insert(opt: InsertOptions): Promise<any> {
        if (!opt.objects) {
            const e = new GenericError("NoObjects", "No objects provided to insert.");
            if (opt.errorHandler) return opt.errorHandler(e);
            else throw e;
        }

        const keys = Object.keys(opt.objects[0]);
        const values: any[] = [];
        const placeholders: string[] = [];

        for (let i = 0; i < opt.objects.length; i++){
            const objValues: any[] = Object.values(opt.objects[i]);
            const tmp: string[] = [];
            for (const v of objValues) {
                tmp.push("$"+(values.length+1));
                values.push(v);
            }
            placeholders.push("("+tmp.join(",")+")");
        }

        let q = util.format("INSERT INTO %s (%s) VALUES %s", opt.table, keys.join(","), placeholders.join(","));

        if(opt.conflictIgnoreOn) {
            q += util.format(" ON CONFLICT (%s) DO NOTHING", opt.conflictIgnoreOn);
        } else if(opt.conflictUpdateOn && opt.objects.length == 1) {
            const id = opt.conflictUpdateOn;
            const obj = Object.assign({}, opt.objects[0]);
            const idVal = obj[id];
            delete obj[id];
            const updateClause = this.prepareUpdateClause(values, obj, opt.table+"."+id+"=$"+(values.length+1), [idVal]);
            q += util.format(" ON CONFLICT (%s) DO UPDATE %s", id, updateClause);
        }
        if (opt.returning){
            q += " RETURNING "+opt.returning;
        }

        return await this.query({
            client: opt.client,
            query: q,
            values: values,
            errorHandler: opt.errorHandler
        });
    }

    /**
     * Update a row(s) in the table.
     *
     * @param opt: look UpdateOptions interface fro info.
     */
    public async update(opt: UpdateOptions): Promise<any> {
        const values = [];
        const q = this.prepareUpdateClause(values, opt.object, opt.where, opt.whereArgs);
        return await this.query({
            client: opt.client,
            query: `UPDATE ${opt.table} ${q}`,
            values: values
        });
    }

    /**
     * Delete row(s) from the table.
     *
     * @param opt: look DeleteOptions interface for info.
     */
    public async delete(opt: DeleteOptions): Promise<any> {
        if (!opt.values) {
            const e = new GenericError("NoObjects", "No values provided to delete.");
            if (opt.errorHandler) return opt.errorHandler(e);
            else throw e;
        }

        const placeholders: string[] = [];
        for (let i = 0; i < opt.values.length; i++){
            placeholders.push("$"+(i+1));
        }

        const q = util.format("DELETE FROM %s WHERE %s IN (%s)", opt.table, opt.column, placeholders.join(","));
        if (opt.client) {
            return await opt.client.query(q, opt.values);
        } else {
            return await this.pool.query(q, opt.values);
        }
    }

    /**
     * Truncate table (DELETE ALL rows from the table).
     *
     * @param table: table name.
     * @param client: execute a query on this client, if null, one will be acquired from pool.
     */
    public async truncateTable(table: string, client?: PoolClient): Promise<any> {
        const q = `TRUNCATE TABLE ${table}`;
        if (client) return await client.query(q);
        else return await this.pool.query(q);
    }

}


export interface BaseOptions {
    // execute a query on this client, if null, one will be acquired from pool
    client?: PoolClient;
    // What should query return when an exception is thrown.
    errorHandler?: (e: Error) => any;
}

export interface QueryOptions extends BaseOptions {
    // query string
    query: string;
    // array of values for query parameters
    values?: any[];
    // return rows as list of Models instead of list of generic objects
    model?: {new(obj): Model;};
    // what type of data to return from query
    fetch?: "all" | "first" | "rows" | "count" | "cursor";
}

export interface InsertOptions extends BaseOptions {
    // table in which to insert data
    table: string;
    // array of objects to inset (object properties must match column names)
    objects: object[],
    // if a conflict occurs on that column, it will be ignored (no row inserted)
    conflictIgnoreOn?: string;
    // If a conflict occurs on that column, a row will be updated with data from objects[0].
    // Only works when inserting exactly one object (basically upsert action).
    conflictUpdateOn?: string;
    // what column(s) should query return after insert
    returning?: string;
}

export interface UpdateOptions extends BaseOptions {
    // table in which to update data
    table: string;
    // object / model to update
    object: object;
    // where query clause
    where?: string;
    // array of values for where clause arguments
    whereArgs?: any[];
}

export interface DeleteOptions extends BaseOptions {
    // table in which to delete data
    table: string;
    // column name to compare
    column: string,
    // values to delete: DELETE FROM table WHERE column IN (values)
    values: any[]
}

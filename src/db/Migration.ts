import * as fs from "fs";
import { PoolClient } from "pg";
import Database from "./Database";

export default abstract class Migration {

    /**
     * If true, migrate method will be executed inside a transaction.
     * Either it completes successfully, or it is rolled back and migration is aborted.
     */
    public transactionMode(): boolean {
        return true;
    }

    /**
     * Execute an .sql file.
     *
     * @param client
     * @param file: path to .sql file.
     */
    public async executeSqlFile(client: PoolClient, file: string): Promise<void> {
        const sql = fs.readFileSync(file).toString();
        await client.query(sql);
    }

    /**
     * Execute migration.
     *
     * @param client: Every query should be executed by this client to ensure the migration is atomic,
     *                except if method transactionMode() returns false.
     * @param db: Database instance with useful methods.
     */
    public async abstract migrate(client: PoolClient, db: Database): Promise<void>;

}

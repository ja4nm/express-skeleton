import Model from "../Model";

export default class User extends Model {

    public id: number;
    public username: string;
    public password: string;

    public hello(): void {
        console.log("Hello "+this.username+"!");
    }

}


import Migration from "../Migration";
import { PoolClient } from "pg";
import Database from "../Database";

export const migration = new class extends Migration {

    async migrate(client: PoolClient, db: Database): Promise<void> {
        await db.query({
            client: client,
            query: "ALTER TABLE users ADD COLUMN password VARCHAR "
        });
    }

};

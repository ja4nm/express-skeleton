import Database from "./Database";
import * as fs from "fs";
import * as path from "path";
import GenericError from "../main/errors/GenericError";
import { PoolClient } from "pg";
import Migration from "./Migration";
import * as util from "util";
import Utl from "../main/Utl";
import Model from "./Model";
import * as sha256File from "sha256-file";

export default class MigrationManager {

    // script file regex: [5 digits]_[filename][.js|.ts|.sql] (.js because .ts is compiled to .ts)
    private static readonly FILENAME_REGEX = new RegExp("^([0-9]{5})[\\-_]([a-zA-Z0-9\\-_\\.]+)\\.(ts|js|sql)$");

    private readonly migrationTable: string;
    private readonly dir: string;
    private readonly db: Database;
    private verbose: boolean;

    /**
     * Initialise database migration manager.
     *
     * @param db: Database instance
     * @param dir: directory with migration script files.
     * @param table: name of the table with migrations info.
     */
    constructor(db: Database, dir: string, table = "_migrations") {
        this.db = db;
        this.dir = dir;
        this.migrationTable = table;
        this.verbose = true;
    }


    private log(msg: string): void {
        if (this.verbose) console.log(msg);
    }

    private generateHash(file: string): string {
        return sha256File(file);
    }

    /**
     * Read directory with migration scripts and return MigrationScript objects
     * in order in which they must be executed.
     */
    private getMigrationScripts(): MigrationScript[] {
        const files = fs.readdirSync(this.dir);
        const map = new Map();

        for(let i = 0; i < files.length; i++){
            const parts = MigrationManager.FILENAME_REGEX.exec(files[i]);
            // filename does not match regex, skip it
            if(!parts) continue;

            // get migration script info
            const file = path.join(this.dir, files[i]);
            const hash = this.generateHash(file);
            const script: MigrationScript = {
                id: parseInt(parts[1]),
                name: parts[2],
                type: parts[3],
                filename: parts[0],
                hash: hash
            };

            // Ensure that there is no duplicated migrations scripts.
            // If there exists 00000_aaa.ts and 00000_aaa.sql, .sql file is ignored.
            const tmp = map.get(script.id);
            if(!tmp || tmp.type == "sql" && script.type != "sql"){
                map.set(script.id, script);
            } else {
                throw new GenericError("DuplicatedMigration", "Duplicated migration id "+parts[1]+".");
            }
        }

        // convert to array and sort by id
        const sorted = Array.from(map.values());
        sorted.sort((a, b) => { return (a.id < b.id)? -1 : 1; });
        return sorted;
    }

    /**
     * Get list of applied migrations from database (migrationTable).
     */
    private async getAppliedMigrations(client: PoolClient): Promise<MigrationEntry[]> {
        return await this.db.query({
            client: client,
            query: `SELECT * FROM ${this.migrationTable} ORDER BY id ASC`,
            fetch: "rows",
            model: MigrationEntry,
            errorHandler: e => []
        });
    }

    /**
     * Inset new migration entry into migrationTable for given migration script.
     */
    private async insertMigrationEntry(script: MigrationScript, client: PoolClient): Promise<void> {
        const q = `INSERT INTO ${this.migrationTable} (id, name, hash, executed_at) VALUES ($1, $2, $3, $4)`;
        const values = [script.id, script.name, script.hash, new Date()];
        await client.query(q, values);
    }

    /**
     * Import migration script, execute it and save migration entry to migrationTable.
     */
    private async executeMigration(script: MigrationScript, client: PoolClient): Promise<void> {
        const file = path.join(this.dir, script.filename);
        let migration;

        // import migration script as Migration object
        if(script.type == "sql"){
            migration = new SimpleFileMigration(file);
        } else {
            migration = (await import(file)).migration;
        }

        if(migration.transactionMode()){
            // execute migration as atomic transaction
            try {
                await client.query("BEGIN");
                await migration.migrate(client, this.db);
                await this.insertMigrationEntry(script, client);
                await client.query("COMMIT");
            }catch (e) {
                await client.query("ROLLBACK");
                throw e;
            }
        }else {
            // execute migration without transaction
            await migration.migrate(client, this.db);
            await this.insertMigrationEntry(script, client);
        }
    }


    /**
     * Set to true to enable detailed logs.
     */
    public setVerbose(verbose: boolean): void {
        this.verbose = verbose;
    }

    /**
     * Get last executed migration from database.
     */
    public async getLastAppliedMigration(): Promise<MigrationEntry> {
        return await this.db.query({
            query: `SELECT * FROM ${this.migrationTable} ORDER BY id DESC LIMIT 1`,
            model: MigrationEntry,
            fetch: "first"
        });
    }

    /**
     * Migrate the database. Validate old migration scripts and the execute new migrations.
     *
     * @return: true if all migrations were successful.
     */
    public async migrate(): Promise<boolean> {
        let ok = true;
        let client;
        let migrations: MigrationScript[] = [];
        let applied: MigrationEntry[] = [];

        // connect to db, get list of migration scripts and list of
        // already executed (applied) migrations
        try {
            client = await this.db.connect();
            await new InitialMigration(this.migrationTable).migrate(client, this.db);
            migrations = this.getMigrationScripts();
            applied = await this.getAppliedMigrations(client);
        }catch (e) {
            this.log("error | "+e.toString());
            if (client) client.release();
            return false;
        }

        // get max migration name length, to format log table
        let maxNameLen = 0;
        for(let i = 0; i < migrations.length; i++){
            if(migrations[i].name.length > maxNameLen) maxNameLen = migrations[i].name.length;
        }

        // validate old migrations and execute new ones
        for(let i = 0; i < migrations.length; i++){
            const m: MigrationScript = migrations[i];
            let msg = util.format("%s | %s | ", Utl.padLeft(""+m.id, 5, "0"), Utl.padRight(m.name, maxNameLen));

            // previous migration failed, skip all later migrations
            if(!ok){
                msg += "skipped";
                this.log(msg);
                continue;
            }
            ok = false;

            if(i < applied.length){
                // migration was already executed, validate it
                const a: MigrationEntry = applied[i];
                if (a.id !== m.id){
                    msg += "validation FAILED (id mismatch)";
                } else if (a.hash !== m.hash) {
                    // migration script was changed
                    msg += "validation FAILED (hash mismatch)";
                } else {
                    msg += "validation OK";
                    ok = true;
                }
            } else {
                // execute new migration
                try{
                    await this.executeMigration(m, client);
                    msg += "migration OK";
                    ok = true;
                }catch (e) {
                    msg += `migration failed: ${e.toString()}`;
                }
            }

            this.log(msg);
        }

        client.release();
        return ok;
    }

}


interface MigrationScript {
    id: number;
    name: string;
    type: string;
    filename: string;
    hash: string;
}

class MigrationEntry extends Model {
    public id: number;
    public name: string;
    public hash: string;
    public executed_at: Date;
}

/**
 * Initial migration creates the migration table if it doesnt exist.
 *
 * @param table: migration table name.
 */
class InitialMigration extends Migration {
    private readonly table: string;

    constructor(table: string){
        super();
        this.table = table;
    }

    async migrate(client: PoolClient, db: Database): Promise<void> {
        const sql = `
            CREATE TABLE IF NOT EXISTS ${this.table} (
                id INT PRIMARY KEY,
                name VARCHAR(100),
                hash VARCHAR(255),
                executed_at TIMESTAMP 
            );
        `;

        await client.query(sql);
    }
}

/**
 * Simple migration which executes script in .sql file.
 *
 * @param file: path to .sql file.
 */
class SimpleFileMigration extends Migration {
    private readonly file: string;

    constructor(file: string) {
        super();
        this.file = file;
    }

    async migrate(client: PoolClient, db: Database): Promise<void> {
        await this.executeSqlFile(client, this.file);
    }
}

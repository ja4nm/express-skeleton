#!/bin/bash
#
# Tool for executing common docker commands.
#

CMD=$1

if [[ $CMD == "local" ]]; then
    docker-compose -f compose-local.yml up
elif [[ $CMD == "dev" ]]; then
    echo "not implemented ..."
elif [[ $CMD == "test" ]]; then
    docker-compose -f compose-test.yml rm -f && docker-compose -f compose-test.yml up --abort-on-container-exit --exit-code-from skeleton_node
elif [[ $CMD == "shell" ]]; then
    docker exec -ti skeleton_node /bin/bash
else
    echo "Help: ./docker-tools.sh [ local | dev | test | shell ]"
fi

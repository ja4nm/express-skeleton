import * as path from "path";
import * as fs from "fs";
import {routesConfig} from "../src/routes";
import { app } from "../src/App";

const testDir: string = path.join(__dirname);

// include tests
fs.readdirSync(testDir).forEach(file => {
    if (file.includes(".test.ts") ) {
        require(`./${file}`);
    }
});

// global setup
before((done) => {
    console.log("\n========== STARTING SERVER ==========\n");
    app.start(routesConfig).then((addr) => {
        console.log(`API is now listening on address ${addr}`);
    }).finally(() => {
        console.log("\n========== RUNNING TESTS ==========\n");
        done();
    });
});

// global teardown
after((done) => {
    app.stop().finally(() => {
        console.log("\n========== TESTS FINISHED ==========\n");
        done();
    });
});

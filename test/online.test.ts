import chaiHttp = require("chai-http");
import * as chai from "chai";
import { expect } from "chai";
import { app } from "../src/App";

chai.use(chaiHttp);

describe("Online test", () => {

    /**
     * Just a sample test.
     */
    it("should add 1 + 1", () => {
        expect(1 + 1).to.equal(2);
    });

    /**
     * Check if api is online.
     */
    it("/ping should return \"pong\"", async () => {
        const r = await chai.request(app.baseUrl)
            .get("/ping");

        expect(r.status).to.equal(200);
        expect(r.text).to.equal("pong");
    });

});
